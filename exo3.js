import {createAll} from './users.data';

const companies = createAll();

console.log('----EXO 3---', companies);

// Créer une fonction qui prend en paramètre la variable "companies"
// un age et qui enlève de toutes les
// companies les users qui ont moins que cet age
function filteruserByAge(companies, age) {
  return companies.map((companie) => {
    companie.users = companie.users.filter((user) => user.age >= age);
    companie.size = companie.users.length;
    return companie;
  });
}
console.log(filteruserByAge(companies, 30));

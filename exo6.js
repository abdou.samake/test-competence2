import {createAll} from './users.data';

const companies = createAll();
console.log('----EXO 6---', companies);

// créer une fonction qui prend en paramètre la variable 'companies' et qui renvoie
// un objet dont les attributs sont la concaténation du nom, du prénom
// et de l'age des users de toutes les companies et dont la valeur
// est la valeur du boolean "car".
function returnObject(companies) {
  const myObject = {};
  companies.map((hotel) => hotel.users
      .forEach((user) => myObject[user.firstName + user.lastName + user.age] = user.car));
  return myObject;
}
console.log(returnObject(companies));



import {createAll} from './users.data';

const companies = createAll();
console.log('----EXO 5---', companies);
// fonction qui renvoi un boolean true si le nombre de user
// qui a un car est superieur ou égale a un parametre cars ou
// false dans le cas contraire.
function ex05(companies, cars) {
  return companies.every((companie) => {
    const numberUserCar = companie.users.reduce(
        (acc, user) => acc = acc + (user.car===true ? + 1: 0), 0);
    return numberUserCar > cars;
  });
}
console.log(ex05(companies, 20));

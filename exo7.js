import {createAll} from './users.data';

const companies = createAll();

console.log('----EXO 7---', companies);

// créer une fonction qui prend en paramètre la variable "companies" et qui
// renvoi un array contenant des objets dont les attributs sont les valeurs des attributs
// des companies et les valeurs la concaténation de tous les prénoms des users

const example = [
  {
    Tempur0: 'BernardBiancaMichel',
    13: 'BernardBiancaMichel',
  }, {
    Renault0: 'JoeJetteRillette',
    19: 'JoeJetteRillette',
  },
];

console.log(example);

// function arrayContainObject(companies) {
//  const myObject = {};
//  return companies.map((companie) => {
//   Object.values(companie).forEach((value) => {
//     if (typeof value !== 'object') {
//       myObject[value] = companie.users.reduce((acc, user) => acc = acc + user.firstName + user.lastName, '');
//      }
//    });
//    return myObject;
// });
// }
// console.log(arrayContainObject(companies));
function arrayObject(companies) {
  const myObject = {};
  return companies.map((companie) => {
    Object.values(companie).forEach((value) => {
      if (typeof value !== 'object') {
        myObject[value] = companie.users.reduce((acc, user) => acc = acc + user.firstName + user.lastName, '');
      }
    });
    return myObject;
  });
}
console.log(arrayObject(companies));

import {createAll} from './users.data';
const companies = createAll();

console.log('----EXO 8---', companies);

// Créer une fonction qui prend en paramètre la variable "companies" et qui calcule
// la moyenne d'âge de tous les users de toutes les companies (utilisation de reduce obligatoire)
// function summMeanAge(companies) {
//  const UsersLength = companies.reduce((acc, companie) => acc = acc + companie.size, 0);
//  const summUserAge = companies.reduce((acc, companie) => acc.concat(companie.users), [])
//      .reduce((acc, user) => acc = acc + user.age, 0);
//  return (summUserAge / UsersLength).toFixed(2);
// }
// console.log(summMeanAge(companies));

function averageAgeUsers(companies) {
  const allUsers = companies.reduce((acc, companie) => acc = acc + companie.size, 0);
  const allAgeUser = companies.reduce((acc, companie) => acc.concat(companie.users), [])
      .reduce((acc, user) => acc = acc + user.age, 0);
  return (allAgeUser / allUsers).toFixed(2);
}
console.log(averageAgeUsers(companies));

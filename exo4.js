import {createAll} from './users.data';
const companies = createAll();

console.log('----EXO 4---', companies);

// Créer une fonction qui prend en paramètre la variable "companies", une variable
// "car (boolean) et une variable age (number)
// qui récupères toutes les "users" de chaque
// company dans un seul tableau, puis qui mets en majuscule à tous leurs
// noms et prénom, puis qui ne garde que les personnes qui ont plus que le paramètre age
// et qui correspondent au paramètre car
function capi(name) {
  return typeof name === 'string' ? name.charAt(0).toUpperCase() + name.slice(1) : '';
}
function filteruserByAgeAnCar(companies, car, age) {
  return companies.reduce((acc, companie) => acc.concat(companie.users), [])
      .map((user) => {
        user.firstName = capi(user.firstName);
        user.lastNmae = capi(user.lastName);
        return user;
      })
      .filter((user) => user.car === car && user.age > age);
}
console.log(filteruserByAgeAnCar(companies, true, 30));
